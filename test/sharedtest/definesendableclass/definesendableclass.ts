/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @tc.name:definesendableclass
 * @tc.desc:test definesendableclass
 * @tc.type: FUNC
 * @tc.require: issueI8R6LC
 */

// @ts-nocheck
declare function print(str: any): string;

class Parent {
    static sField: number = 0;
    field: number = 0;

    static get sAge() {
        print("sAge getter");
        return 0;
    }
    static set sAge(a: number) {
        print("sAge setter");
    }
    get age() {
        print("age getter");
        return 0;
    }
    set age(a: number) {
        print("age setter");
    }
    constructor() {
        "use sendable";
    }
}

class Child extends Parent {
    childField: number = 0;

    constructor() {
        "use sendable";
        super();
    }
}

class ChildToDict extends Parent {
    a0:string = "";
    a1:string = "";
    a2:string = "";
    a3:string = "";
    a4:string = "";
    a5:string = "";
    a6:string = "";
    a7:string = "";
    a8:string = "";
    a9:string = "";
    a10:string = "";
    a11:string = "";
    a12:string = "";
    a13:string = "";
    a14:string = "";
    a15:string = "";
    a16:string = "";
    a17:string = "";
    a18:string = "";
    a19:string = "";
    a20:string = "";
    a21:string = "";
    a22:string = "";
    a23:string = "";
    a24:string = "";
    a25:string = "";
    a26:string = "";
    a27:string = "";
    a28:string = "";
    a29:string = "";
    a30:string = "";
    a31:string = "";
    a32:string = "";
    a33:string = "";
    a34:string = "";
    a35:string = "";
    a36:string = "";
    a37:string = "";
    a38:string = "";
    a39:string = "";
    a40:string = "";
    a41:string = "";
    a42:string = "";
    a43:string = "";
    a44:string = "";
    a45:string = "";
    a46:string = "";
    a47:string = "";
    a48:string = "";
    a49:string = "";
    a50:string = "";
    a51:string = "";
    a52:string = "";
    a53:string = "";
    a54:string = "";
    a55:string = "";
    a56:string = "";
    a57:string = "";
    a58:string = "";
    a59:string = "";
    a60:string = "";
    a61:string = "";
    a62:string = "";
    a63:string = "";
    a64:string = "";
    a65:string = "";
    a66:string = "";
    a67:string = "";
    a68:string = "";
    a69:string = "";
    a70:string = "";
    a71:string = "";
    a72:string = "";
    a73:string = "";
    a74:string = "";
    a75:string = "";
    a76:string = "";
    a77:string = "";
    a78:string = "";
    a79:string = "";
    a80:string = "";
    a81:string = "";
    a82:string = "";
    a83:string = "";
    a84:string = "";
    a85:string = "";
    a86:string = "";
    a87:string = "";
    a88:string = "";
    a89:string = "";
    a90:string = "";
    a91:string = "";
    a92:string = "";
    a93:string = "";
    a94:string = "";
    a95:string = "";
    a96:string = "";
    a97:string = "";
    a98:string = "";
    a99:string = ""; 
    a100:string = "";
    a101:string = "";
    a102:string = "";
    a103:string = "";
    a104:string = "";
    a105:string = "";
    a106:string = "";
    a107:string = "";
    a108:string = "";
    a109:string = "";
    a110:string = "";
    a111:string = "";
    a112:string = "";
    a113:string = "";
    a114:string = "";
    a115:string = "";
    a116:string = "";
    a117:string = "";
    a118:string = "";
    a119:string = "";
    a120:string = "";
    a121:string = "";
    a122:string = "";
    a123:string = "";
    a124:string = "";
    a125:string = "";
    a126:string = "";
    a127:string = "";
    a128:string = "";
    a129:string = "";
    a130:string = "";
    a131:string = "";
    a132:string = "";
    a133:string = "";
    a134:string = "";
    a135:string = "";
    a136:string = "";
    a137:string = "";
    a138:string = "";
    a139:string = "";
    a140:string = "";
    a141:string = "";
    a142:string = "";
    a143:string = "";
    a144:string = "";
    a145:string = "";
    a146:string = "";
    a147:string = "";
    a148:string = "";
    a149:string = "";
    a150:string = "";
    a151:string = "";
    a152:string = "";
    a153:string = "";
    a154:string = "";
    a155:string = "";
    a156:string = "";
    a157:string = "";
    a158:string = "";
    a159:string = "";
    a160:string = "";
    a161:string = "";
    a162:string = "";
    a163:string = "";
    a164:string = "";
    a165:string = "";
    a166:string = "";
    a167:string = "";
    a168:string = "";
    a169:string = "";
    a170:string = "";
    a171:string = "";
    a172:string = "";
    a173:string = "";
    a174:string = "";
    a175:string = "";
    a176:string = "";
    a177:string = "";
    a178:string = "";
    a179:string = "";
    a180:string = "";
    a181:string = "";
    a182:string = "";
    a183:string = "";
    a184:string = "";
    a185:string = "";
    a186:string = "";
    a187:string = "";
    a188:string = "";
    a189:string = "";
    a190:string = "";
    a191:string = "";
    a192:string = "";
    a193:string = "";
    a194:string = "";
    a195:string = "";
    a196:string = "";
    a197:string = "";
    a198:string = "";
    a199:string = "";
    a200:string = "";
    a201:string = "";
    a202:string = "";
    a203:string = "";
    a204:string = "";
    a205:string = "";
    a206:string = "";
    a207:string = "";
    a208:string = "";
    a209:string = "";
    a210:string = "";
    a211:string = "";
    a212:string = "";
    a213:string = "";
    a214:string = "";
    a215:string = "";
    a216:string = "";
    a217:string = "";
    a218:string = "";
    a219:string = "";
    a220:string = "";
    a221:string = "";
    a222:string = "";
    a223:string = "";
    a224:string = "";
    a225:string = "";
    a226:string = "";
    a227:string = "";
    a228:string = "";
    a229:string = "";
    a230:string = "";
    a231:string = "";
    a232:string = "";
    a233:string = "";
    a234:string = "";
    a235:string = "";
    a236:string = "";
    a237:string = "";
    a238:string = "";
    a239:string = "";
    a240:string = "";
    a241:string = "";
    a242:string = "";
    a243:string = "";
    a244:string = "";
    a245:string = "";
    a246:string = "";
    a247:string = "";
    a248:string = "";
    a249:string = "";
    a250:string = "";
    a251:string = "";
    a252:string = "";
    a253:string = "";
    a254:string = "";
    a255:string = "";
    a256:string = "";
    a257:string = "";
    a258:string = "";
    a259:string = "";
    a260:string = "";
    a261:string = "";
    a262:string = "";
    a263:string = "";
    a264:string = "";
    a265:string = "";
    a266:string = "";
    a267:string = "";
    a268:string = "";
    a269:string = "";
    a270:string = "";
    a271:string = "";
    a272:string = "";
    a273:string = "";
    a274:string = "";
    a275:string = "";
    a276:string = "";
    a277:string = "";
    a278:string = "";
    a279:string = "";
    a280:string = "";
    a281:string = "";
    a282:string = "";
    a283:string = "";
    a284:string = "";
    a285:string = "";
    a286:string = "";
    a287:string = "";
    a288:string = "";
    a289:string = "";
    a290:string = "";
    a291:string = "";
    a292:string = "";
    a293:string = "";
    a294:string = "";
    a295:string = "";
    a296:string = "";
    a297:string = "";
    a298:string = "";
    a299:string = "";
    a300:string = "";
    a301:string = "";
    a302:string = "";
    a303:string = "";
    a304:string = "";
    a305:string = "";
    a306:string = "";
    a307:string = "";
    a308:string = "";
    a309:string = "";
    a310:string = "";
    a311:string = "";
    a312:string = "";
    a313:string = "";
    a314:string = "";
    a315:string = "";
    a316:string = "";
    a317:string = "";
    a318:string = "";
    a319:string = "";
    a320:string = "";
    a321:string = "";
    a322:string = "";
    a323:string = "";
    a324:string = "";
    a325:string = "";
    a326:string = "";
    a327:string = "";
    a328:string = "";
    a329:string = "";
    a330:string = "";
    a331:string = "";
    a332:string = "";
    a333:string = "";
    a334:string = "";
    a335:string = "";
    a336:string = "";
    a337:string = "";
    a338:string = "";
    a339:string = "";
    a340:string = "";
    a341:string = "";
    a342:string = "";
    a343:string = "";
    a344:string = "";
    a345:string = "";
    a346:string = "";
    a347:string = "";
    a348:string = "";
    a349:string = "";
    a350:string = "";
    a351:string = "";
    a352:string = "";
    a353:string = "";
    a354:string = "";
    a355:string = "";
    a356:string = "";
    a357:string = "";
    a358:string = "";
    a359:string = "";
    a360:string = "";
    a361:string = "";
    a362:string = "";
    a363:string = "";
    a364:string = "";
    a365:string = "";
    a366:string = "";
    a367:string = "";
    a368:string = "";
    a369:string = "";
    a370:string = "";
    a371:string = "";
    a372:string = "";
    a373:string = "";
    a374:string = "";
    a375:string = "";
    a376:string = "";
    a377:string = "";
    a378:string = "";
    a379:string = "";
    a380:string = "";
    a381:string = "";
    a382:string = "";
    a383:string = "";
    a384:string = "";
    a385:string = "";
    a386:string = "";
    a387:string = "";
    a388:string = "";
    a389:string = "";
    a390:string = "";
    a391:string = "";
    a392:string = "";
    a393:string = "";
    a394:string = "";
    a395:string = "";
    a396:string = "";
    a397:string = "";
    a398:string = "";
    a399:string = "";
    a400:string = "";
    a401:string = "";
    a402:string = "";
    a403:string = "";
    a404:string = "";
    a405:string = "";
    a406:string = "";
    a407:string = "";
    a408:string = "";
    a409:string = "";
    a410:string = "";
    a411:string = "";
    a412:string = "";
    a413:string = "";
    a414:string = "";
    a415:string = "";
    a416:string = "";
    a417:string = "";
    a418:string = "";
    a419:string = "";
    a420:string = "";
    a421:string = "";
    a422:string = "";
    a423:string = "";
    a424:string = "";
    a425:string = "";
    a426:string = "";
    a427:string = "";
    a428:string = "";
    a429:string = "";
    a430:string = "";
    a431:string = "";
    a432:string = "";
    a433:string = "";
    a434:string = "";
    a435:string = "";
    a436:string = "";
    a437:string = "";
    a438:string = "";
    a439:string = "";
    a440:string = "";
    a441:string = "";
    a442:string = "";
    a443:string = "";
    a444:string = "";
    a445:string = "";
    a446:string = "";
    a447:string = "";
    a448:string = "";
    a449:string = "";
    a450:string = "";
    a451:string = "";
    a452:string = "";
    a453:string = "";
    a454:string = "";
    a455:string = "";
    a456:string = "";
    a457:string = "";
    a458:string = "";
    a459:string = "";
    a460:string = "";
    a461:string = "";
    a462:string = "";
    a463:string = "";
    a464:string = "";
    a465:string = "";
    a466:string = "";
    a467:string = "";
    a468:string = "";
    a469:string = "";
    a470:string = "";
    a471:string = "";
    a472:string = "";
    a473:string = "";
    a474:string = "";
    a475:string = "";
    a476:string = "";
    a477:string = "";
    a478:string = "";
    a479:string = "";
    a480:string = "";
    a481:string = "";
    a482:string = "";
    a483:string = "";
    a484:string = "";
    a485:string = "";
    a486:string = "";
    a487:string = "";
    a488:string = "";
    a489:string = "";
    a490:string = "";
    a491:string = "";
    a492:string = "";
    a493:string = "";
    a494:string = "";
    a495:string = "";
    a496:string = "";
    a497:string = "";
    a498:string = "";
    a499:string = "";
    a500:string = "";
    a501:string = "";
    a502:string = "";
    a503:string = "";
    a504:string = "";
    a505:string = "";
    a506:string = "";
    a507:string = "";
    a508:string = "";
    a509:string = "";
    a510:string = "";
    a511:string = "";
    a512:string = "";
    a513:string = "";
    a514:string = "";
    a515:string = "";
    a516:string = "";
    a517:string = "";
    a518:string = "";
    a519:string = "";
    a520:string = "";
    a521:string = "";
    a522:string = "";
    a523:string = "";
    a524:string = "";
    a525:string = "";
    a526:string = "";
    a527:string = "";
    a528:string = "";
    a529:string = "";
    a530:string = "";
    a531:string = "";
    a532:string = "";
    a533:string = "";
    a534:string = "";
    a535:string = "";
    a536:string = "";
    a537:string = "";
    a538:string = "";
    a539:string = "";
    a540:string = "";
    a541:string = "";
    a542:string = "";
    a543:string = "";
    a544:string = "";
    a545:string = "";
    a546:string = "";
    a547:string = "";
    a548:string = "";
    a549:string = "";
    a550:string = "";
    a551:string = "";
    a552:string = "";
    a553:string = "";
    a554:string = "";
    a555:string = "";
    a556:string = "";
    a557:string = "";
    a558:string = "";
    a559:string = "";
    a560:string = "";
    a561:string = "";
    a562:string = "";
    a563:string = "";
    a564:string = "";
    a565:string = "";
    a566:string = "";
    a567:string = "";
    a568:string = "";
    a569:string = "";
    a570:string = "";
    a571:string = "";
    a572:string = "";
    a573:string = "";
    a574:string = "";
    a575:string = "";
    a576:string = "";
    a577:string = "";
    a578:string = "";
    a579:string = "";
    a580:string = "";
    a581:string = "";
    a582:string = "";
    a583:string = "";
    a584:string = "";
    a585:string = "";
    a586:string = "";
    a587:string = "";
    a588:string = "";
    a589:string = "";
    a590:string = "";
    a591:string = "";
    a592:string = "";
    a593:string = "";
    a594:string = "";
    a595:string = "";
    a596:string = "";
    a597:string = "";
    a598:string = "";
    a599:string = "";
    a600:string = "";
    a601:string = "";
    a602:string = "";
    a603:string = "";
    a604:string = "";
    a605:string = "";
    a606:string = "";
    a607:string = "";
    a608:string = "";
    a609:string = "";
    a610:string = "";
    a611:string = "";
    a612:string = "";
    a613:string = "";
    a614:string = "";
    a615:string = "";
    a616:string = "";
    a617:string = "";
    a618:string = "";
    a619:string = "";
    a620:string = "";
    a621:string = "";
    a622:string = "";
    a623:string = "";
    a624:string = "";
    a625:string = "";
    a626:string = "";
    a627:string = "";
    a628:string = "";
    a629:string = "";
    a630:string = "";
    a631:string = "";
    a632:string = "";
    a633:string = "";
    a634:string = "";
    a635:string = "";
    a636:string = "";
    a637:string = "";
    a638:string = "";
    a639:string = "";
    a640:string = "";
    a641:string = "";
    a642:string = "";
    a643:string = "";
    a644:string = "";
    a645:string = "";
    a646:string = "";
    a647:string = "";
    a648:string = "";
    a649:string = "";
    a650:string = "";
    a651:string = "";
    a652:string = "";
    a653:string = "";
    a654:string = "";
    a655:string = "";
    a656:string = "";
    a657:string = "";
    a658:string = "";
    a659:string = "";
    a660:string = "";
    a661:string = "";
    a662:string = "";
    a663:string = "";
    a664:string = "";
    a665:string = "";
    a666:string = "";
    a667:string = "";
    a668:string = "";
    a669:string = "";
    a670:string = "";
    a671:string = "";
    a672:string = "";
    a673:string = "";
    a674:string = "";
    a675:string = "";
    a676:string = "";
    a677:string = "";
    a678:string = "";
    a679:string = "";
    a680:string = "";
    a681:string = "";
    a682:string = "";
    a683:string = "";
    a684:string = "";
    a685:string = "";
    a686:string = "";
    a687:string = "";
    a688:string = "";
    a689:string = "";
    a690:string = "";
    a691:string = "";
    a692:string = "";
    a693:string = "";
    a694:string = "";
    a695:string = "";
    a696:string = "";
    a697:string = "";
    a698:string = "";
    a699:string = "";
    a700:string = "";
    a701:string = "";
    a702:string = "";
    a703:string = "";
    a704:string = "";
    a705:string = "";
    a706:string = "";
    a707:string = "";
    a708:string = "";
    a709:string = "";
    a710:string = "";
    a711:string = "";
    a712:string = "";
    a713:string = "";
    a714:string = "";
    a715:string = "";
    a716:string = "";
    a717:string = "";
    a718:string = "";
    a719:string = "";
    a720:string = "";
    a721:string = "";
    a722:string = "";
    a723:string = "";
    a724:string = "";
    a725:string = "";
    a726:string = "";
    a727:string = "";
    a728:string = "";
    a729:string = "";
    a730:string = "";
    a731:string = "";
    a732:string = "";
    a733:string = "";
    a734:string = "";
    a735:string = "";
    a736:string = "";
    a737:string = "";
    a738:string = "";
    a739:string = "";
    a740:string = "";
    a741:string = "";
    a742:string = "";
    a743:string = "";
    a744:string = "";
    a745:string = "";
    a746:string = "";
    a747:string = "";
    a748:string = "";
    a749:string = "";
    a750:string = "";
    a751:string = "";
    a752:string = "";
    a753:string = "";
    a754:string = "";
    a755:string = "";
    a756:string = "";
    a757:string = "";
    a758:string = "";
    a759:string = "";
    a760:string = "";
    a761:string = "";
    a762:string = "";
    a763:string = "";
    a764:string = "";
    a765:string = "";
    a766:string = "";
    a767:string = "";
    a768:string = "";
    a769:string = "";
    a770:string = "";
    a771:string = "";
    a772:string = "";
    a773:string = "";
    a774:string = "";
    a775:string = "";
    a776:string = "";
    a777:string = "";
    a778:string = "";
    a779:string = "";
    a780:string = "";
    a781:string = "";
    a782:string = "";
    a783:string = "";
    a784:string = "";
    a785:string = "";
    a786:string = "";
    a787:string = "";
    a788:string = "";
    a789:string = "";
    a790:string = "";
    a791:string = "";
    a792:string = "";
    a793:string = "";
    a794:string = "";
    a795:string = "";
    a796:string = "";
    a797:string = "";
    a798:string = "";
    a799:string = "";
    a800:string = "";
    a801:string = "";
    a802:string = "";
    a803:string = "";
    a804:string = "";
    a805:string = "";
    a806:string = "";
    a807:string = "";
    a808:string = "";
    a809:string = "";
    a810:string = "";
    a811:string = "";
    a812:string = "";
    a813:string = "";
    a814:string = "";
    a815:string = "";
    a816:string = "";
    a817:string = "";
    a818:string = "";
    a819:string = "";
    a820:string = "";
    a821:string = "";
    a822:string = "";
    a823:string = "";
    a824:string = "";
    a825:string = "";
    a826:string = "";
    a827:string = "";
    a828:string = "";
    a829:string = "";
    a830:string = "";
    a831:string = "";
    a832:string = "";
    a833:string = "";
    a834:string = "";
    a835:string = "";
    a836:string = "";
    a837:string = "";
    a838:string = "";
    a839:string = "";
    a840:string = "";
    a841:string = "";
    a842:string = "";
    a843:string = "";
    a844:string = "";
    a845:string = "";
    a846:string = "";
    a847:string = "";
    a848:string = "";
    a849:string = "";
    a850:string = "";
    a851:string = "";
    a852:string = "";
    a853:string = "";
    a854:string = "";
    a855:string = "";
    a856:string = "";
    a857:string = "";
    a858:string = "";
    a859:string = "";
    a860:string = "";
    a861:string = "";
    a862:string = "";
    a863:string = "";
    a864:string = "";
    a865:string = "";
    a866:string = "";
    a867:string = "";
    a868:string = "";
    a869:string = "";
    a870:string = "";
    a871:string = "";
    a872:string = "";
    a873:string = "";
    a874:string = "";
    a875:string = "";
    a876:string = "";
    a877:string = "";
    a878:string = "";
    a879:string = "";
    a880:string = "";
    a881:string = "";
    a882:string = "";
    a883:string = "";
    a884:string = "";
    a885:string = "";
    a886:string = "";
    a887:string = "";
    a888:string = "";
    a889:string = "";
    a890:string = "";
    a891:string = "";
    a892:string = "";
    a893:string = "";
    a894:string = "";
    a895:string = "";
    a896:string = "";
    a897:string = "";
    a898:string = "";
    a899:string = "";
    a900:string = "";
    a901:string = "";
    a902:string = "";
    a903:string = "";
    a904:string = "";
    a905:string = "";
    a906:string = "";
    a907:string = "";
    a908:string = "";
    a909:string = "";
    a910:string = "";
    a911:string = "";
    a912:string = "";
    a913:string = "";
    a914:string = "";
    a915:string = "";
    a916:string = "";
    a917:string = "";
    a918:string = "";
    a919:string = "";
    a920:string = "";
    a921:string = "";
    a922:string = "";
    a923:string = "";
    a924:string = "";
    a925:string = "";
    a926:string = "";
    a927:string = "";
    a928:string = "";
    a929:string = "";
    a930:string = "";
    a931:string = "";
    a932:string = "";
    a933:string = "";
    a934:string = "";
    a935:string = "";
    a936:string = "";
    a937:string = "";
    a938:string = "";
    a939:string = "";
    a940:string = "";
    a941:string = "";
    a942:string = "";
    a943:string = "";
    a944:string = "";
    a945:string = "";
    a946:string = "";
    a947:string = "";
    a948:string = "";
    a949:string = "";
    a950:string = "";
    a951:string = "";
    a952:string = "";
    a953:string = "";
    a954:string = "";
    a955:string = "";
    a956:string = "";
    a957:string = "";
    a958:string = "";
    a959:string = "";
    a960:string = "";
    a961:string = "";
    a962:string = "";
    a963:string = "";
    a964:string = "";
    a965:string = "";
    a966:string = "";
    a967:string = "";
    a968:string = "";
    a969:string = "";
    a970:string = "";
    a971:string = "";
    a972:string = "";
    a973:string = "";
    a974:string = "";
    a975:string = "";
    a976:string = "";
    a977:string = "";
    a978:string = "";
    a979:string = "";
    a980:string = "";
    a981:string = "";
    a982:string = "";
    a983:string = "";
    a984:string = "";
    a985:string = "";
    a986:string = "";
    a987:string = "";
    a988:string = "";
    a989:string = "";
    a990:string = "";
    a991:string = "";
    a992:string = "";
    a993:string = "";
    a994:string = "";
    a995:string = "";
    a996:string = "";
    a997:string = "";
    a998:string = "";
    a999:string = "";
    a1000:string = "";
    a1001:string = "";
    a1002:string = "";
    a1003:string = "";
    a1004:string = "";
    a1005:string = "";
    a1006:string = "";
    a1007:string = "";
    a1008:string = "";
    a1009:string = "";
    a1010:string = "";
    a1011:string = "";
    a1012:string = "";
    a1013:string = "";
    a1014:string = "";
    a1015:string = "";
    a1016:string = "";
    a1017:string = "a1017"
    a1018:string = "";
    a1019:string = "";

    constructor() {
        "use sendable";
        super();
    }
}

class ChildExtendsDict extends ChildToDict {
    childField: number = 0;

    constructor() {
        "use sendable";
        super();
    }
}

function testParent(parent: Parent) {
    // test getter/setter
    parent.age;
    parent.age = 1;
    Parent.sAge;
    Parent.sAge = 1;
    // test field
    print("parent.field = " + parent.field);
    print("Parent.sField = " + Parent.sField)
}

function testChild(child: Child) {
    // test base
    testParent(child);
    // test child
    print("child.childField = " + child.childField);
}

function testChildToDict(child: ChildToDict) {
    // test parent
    testParent(child);
    // test childToDict
    print("child.a1017 = " + child.a1017);
}

function testChildExtendsDict(child: ChildExtendsDict) {
    // test parent
    testChildToDict(child);
    // test childExtendsDict
    print("child.childField = " + child.childField);
}

let parent = new Parent;
let child = new Child;
let childToDict = new ChildToDict;
let childExtendsDict = new ChildExtendsDict;

testParent(parent);
testChild(child);
testChildToDict(childToDict);
testChildExtendsDict(childExtendsDict);
